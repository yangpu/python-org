#!/usr/bin/env python3

# Python3 支持 int、float、bool、complex（复数）。
# 在Python 3里，只有一种整数类型 int，表示为长整型，没有 python2 中的 Long。
# 像大多数语言一样，数值类型的赋值和计算都是很直观的。
# 内置的 type() 函数可以用来查询变量所指的对象类型。
a, b, c, d = 20, 5.5, True, 4+3j
print(type(a))
print(type(b))
print(type(c))
print(type(d))


# 此外还可以用 isinstance 来判断
print(isinstance(a, int))



# isinstance 和 type 的区别在于：
# class A:
#     pass

# class B(A):
#     pass

# isinstance(A(), A)  # returns True
# type(A()) == A      # returns True
# isinstance(B(), A)    # returns True
# type(B()) == A        # returns False
# 区别就是:
# type()不会认为子类是一种父类类型。
# isinstance()会认为子类是一种父类类型。


# 1、Python可以同时为多个变量赋值，如a, b = 1, 2。
# 2、一个变量可以通过赋值指向不同类型的对象。
# 3、数值的除法（/）总是返回一个浮点数，要获取整数使用//操作符。
# 4、在混合计算时，Python会把整型转换成为浮点数。

print(5 + 4)
print(4.3 - 2)
print(3 * 7)
print(2 / 4)
print(2 // 4)
print(17 % 3)
print(2 ** 5)