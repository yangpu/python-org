#!/usr/bin/python3
# Python3 迭代器与生成器
list = [1,2,3,4]
it = iter(list)
print(next(it)) # 创建迭代器对象
print(next(it)) # 输出迭代器的下一个元素

# 迭代器对象可以使用常规for语句进行遍历：
for x in it:
    print (x, end=" ")

print('也可以使用 next() 函数：')


import sys         # 引入 sys 模块
while True:
    try:
        print (next(it))
    except StopIteration:
        sys.exit()